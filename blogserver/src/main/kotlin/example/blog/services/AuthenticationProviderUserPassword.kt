package example.blog.services

import example.blog.model.UserLogin
import io.micronaut.security.authentication.AuthenticationFailed
import io.micronaut.security.authentication.AuthenticationProvider
import io.micronaut.security.authentication.AuthenticationRequest
import io.micronaut.security.authentication.AuthenticationResponse
import io.micronaut.security.authentication.UserDetails
import io.reactivex.Flowable
import org.reactivestreams.Publisher
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthenticationProviderUserPassword : AuthenticationProvider {

    @Inject
    protected var userLoginService:UserLoginService? = null

    override fun authenticate(authenticationRequest: AuthenticationRequest<*, *>?): Publisher<AuthenticationResponse> {
        if (authenticationRequest != null && authenticationRequest.identity != null && authenticationRequest.secret != null) {
            val userlogin:Optional<UserLogin>? =  getAuthenticationDetails(authenticationRequest.identity as String)
            if (authenticationRequest.identity == userlogin?.get()?.username && authenticationRequest.secret == userlogin?.get()?.password) {
            //if (authenticationRequest.identity == "sherlock" && authenticationRequest.secret == "password") {
                return Flowable.just<AuthenticationResponse>(UserDetails(authenticationRequest.identity as String, ArrayList()))
            }
        }
        return Flowable.just<AuthenticationResponse>(AuthenticationFailed())
    }

    private fun getAuthenticationDetails(username:String):Optional<UserLogin>?{

        return userLoginService?.findUserCredentials(username)
    }


}
