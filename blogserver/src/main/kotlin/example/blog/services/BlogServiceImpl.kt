package example.blog.services

import example.blog.model.Blog
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession
import io.micronaut.runtime.ApplicationConfiguration
import io.micronaut.spring.tx.annotation.Transactional
import java.util.*
import javax.inject.Singleton
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.Query
import javax.persistence.TypedQuery
import javax.validation.constraints.NotNull

@Singleton
open class BlogServiceImpl: BlogService {

    @PersistenceContext
    private var entityManager:EntityManager? = null
    private var applicationConfiguration:ApplicationConfiguration? = null

    fun BlogServiceImpl(@CurrentSession entityManager: EntityManager?,
                                    applicationConfiguration: ApplicationConfiguration?){
        this.entityManager = entityManager
        this.applicationConfiguration = applicationConfiguration
    }

    @Transactional(readOnly = true)
    override fun findById(id: Long?): Optional<Blog> {
        return Optional.ofNullable(entityManager?.find(Blog::class.java, id))
    }

    @Transactional
    override fun save(blog: @NotNull Blog?): Blog? {
      entityManager?.persist(blog)
        return blog
    }

    @Transactional
    override fun deleteById(id: @NotNull Long?) {
        findById(id).ifPresent{ blog: Blog ->entityManager!!.remove(blog)}
    }

    @Transactional(readOnly = true)
    override fun findAll(): List<Blog?>? {
        val qlString = "Select e FROM  Blog as e"
        val query:TypedQuery<Blog> = entityManager!!.createQuery(qlString, Blog::class.java)

        query.maxResults = 50
        return query.resultList
    }

    @Transactional
    override fun update(id:  @NotNull Long?, title: @NotNull String, subtitle: @NotNull String, content: String): Int? {
        val queryString: String = "UPDATE Blog g SET title = :title, subtitle = :subtitle,content = :content where id = :id"

        val query:Query = entityManager!!.createQuery(queryString)

        query.setParameter("title",title)
        query.setParameter("subtitle",subtitle)
        query.setParameter("content",content)
        query.setParameter("id",id)

        return query.executeUpdate()
    }
}