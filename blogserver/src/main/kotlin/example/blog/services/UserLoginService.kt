package example.blog.services

import example.blog.model.Blog
import example.blog.model.UserLogin
import java.util.*
import javax.validation.constraints.NotNull

interface UserLoginService {
    fun  findUserCredentials(username:@NotNull String?): Optional<UserLogin>

    fun save(userLogin:@NotNull UserLogin?): UserLogin?

}