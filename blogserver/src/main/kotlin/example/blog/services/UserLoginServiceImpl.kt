package example.blog.services

import example.blog.model.UserLogin
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession
import io.micronaut.runtime.ApplicationConfiguration
import io.micronaut.spring.tx.annotation.Transactional
import java.util.*
import javax.inject.Singleton
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.Query
import javax.persistence.TypedQuery

@Singleton
open class UserLoginServiceImpl: UserLoginService {

    @PersistenceContext
    private var entityManager:EntityManager?=null
    private var applicationConfiguration:ApplicationConfiguration?=null

    fun UserLoginServiceImpl(@CurrentSession entityManager: EntityManager?,
                             applicationConfiguration: ApplicationConfiguration){
        this.entityManager = entityManager
        this.applicationConfiguration = applicationConfiguration
    }

    @Transactional(readOnly = true)
    override fun findUserCredentials(username: String?): Optional<UserLogin> {
        val qlString = " FROM UserLogin WHERE username = :username"
        val query:TypedQuery<UserLogin> = entityManager!!.createQuery(qlString,UserLogin::class.java)
        query.setParameter("username",username)

        return Optional.ofNullable(query.resultList.last())
    }

    @Transactional
    override fun save(userLogin: UserLogin?): UserLogin? {
        entityManager?.persist(userLogin)
        return userLogin
    }

}