package example.blog

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("example.blog")
                .mainClass(Application.javaClass)
                .start()
    }
}