package example.blog.controller

import example.blog.model.Blog
import example.blog.services.BlogService
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import java.net.URI
import javax.inject.Inject
import javax.validation.Valid

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller
open class BlogController {

        @Inject
        protected var blogService: BlogService? = null

        @Produces(MediaType.APPLICATION_JSON)
        @Get("/blogs")
        fun index(): List<Blog?>? {
            return blogService?.findAll()
        }

        @Produces(MediaType.APPLICATION_JSON)
        @Get("/blogs/{id}")
        fun show(id: Long): Blog?{
                return blogService?.findById(id)?.orElse(null)
        }

        @Post("/blogs")
        fun save(@Body blog:@Valid Blog):HttpResponse<Blog?>?{
                blogService?.save(blog)
                return HttpResponse
                        .created(blog)
                        .headers{headers -> headers.location(blog?.
                        let{ toUri(blog)})}
        }

        @Put("/blogs/{id}")
        fun update(id:Long, @Body blog:@Valid Blog): Int? {
                return  blog.title?.let {title->blog.subtitle?.let {
                        subtitle ->blog.content?.let { content ->
                        blogService?.update(id,title,subtitle,content)
                }}}
        }

        @Delete("/blogs/{id}")
        open fun delete(id:Long?): HttpResponse<*>?{
                blogService?.deleteById(id)
                return HttpResponse.noContent<Any>()
        }

        private fun toUri(blog: Blog):URI? {
                return URI.create("/blog/" + blog.id)
        }
}

