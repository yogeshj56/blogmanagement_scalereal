package example.blog.controller

import example.blog.model.UserLogin
import example.blog.services.UserLoginService
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import io.micronaut.security.annotation.Secured
import io.micronaut.security.rules.SecurityRule
import java.net.URI
import java.security.Principal
import javax.inject.Inject
import javax.validation.Valid

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller
class LoginController {

    @Inject
    protected var userLoginService: UserLoginService? = null

    @Produces(MediaType.TEXT_PLAIN)
    @Get("/login")
    fun index(principal: Principal): String {

        return principal.toString()
    }

    @Secured(SecurityRule.IS_ANONYMOUS)
    @Post("/logins")
    fun save(@Body userlogin:@Valid UserLogin): HttpResponse<UserLogin?>?{
        userLoginService?.save(userlogin)
        return HttpResponse.created(userlogin)
                .headers{headers->headers.location(URI.create("/userlogin/${userlogin.id}"))}
    }

}
