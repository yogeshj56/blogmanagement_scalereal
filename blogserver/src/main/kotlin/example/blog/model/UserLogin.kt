package example.blog.model

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name="userlogin")
class UserLogin {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id:Long?=null

    @Column(name = "username",nullable = false)
    var username:String?=null

    @Column(name = "password",nullable = false)
    var password:String?=null

    constructor() {}

    constructor(username:@NotNull String?,password: @NotNull String?)
    {
        this.username = username
        this.password = password
    }
}