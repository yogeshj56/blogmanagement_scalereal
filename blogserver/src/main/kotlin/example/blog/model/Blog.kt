package example.blog.model

import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "blogs")
class Blog {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    @Column(name = "title", nullable = false)
    var title: String? = null

    @Column(name = "subtitle", nullable = false)
    var subtitle: @NotNull String? = null

    @Column(name = "content", nullable = false)
    var content: @NotNull String? = null
    constructor() {}

    constructor(title: @NotNull String?, subtitle: @NotNull String?, content: @NotNull String?) {
        this.title = title
        this.subtitle = subtitle
        this.content = content
    }
}
