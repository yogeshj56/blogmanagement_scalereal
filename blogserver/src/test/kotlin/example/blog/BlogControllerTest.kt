package example.blog

import example.blog.model.Blog
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import javax.inject.Inject

class BlogControllerTest {

    @Inject
    @field:Client("/")
    lateinit var  client:HttpClient

    @Test
    fun testBlogCrudOperations() {
        var request:HttpRequest<Any> = HttpRequest.POST("/blogs", Blog("testTitle", "TestSubtitle", "test Content of blog"))
        var response:HttpResponse<Any> = client.toBlocking().exchange(request)
        Assertions.assertEquals(HttpStatus.CREATED,response.status)

        request= HttpRequest.POST("/blogs", Blog("testTitle _ 1", "TestSubtitle _ 1", "test Content of blog _ 1"))
        response= client.toBlocking().exchange(request)
        Assertions.assertEquals(HttpStatus.CREATED,response.status)

       request = HttpRequest.GET("/blogs/1")
       val blg = client.toBlocking().retrieve(request)
        Assertions.assertEquals("{ 'id': 1,'title': 'testTitle','subtitle': 'TestSubtitle','content': 'test Content of blog'}", blg)

        request = HttpRequest.PUT("/blogs/1", Blog("testTitle _ 1_updated", "TestSubtitle _ 1_updated", "test Content of blog _ 1_updated"))
        response= client.toBlocking().exchange(request)
        Assertions.assertEquals(HttpStatus.OK,response.status)

        request = HttpRequest.PUT("/blogs/3", Blog("testTitle _ 1_updated", "TestSubtitle _ 1_updated", "test Content of blog _ 1_updated"))
        response= client.toBlocking().exchange(request)
        Assertions.assertEquals(0,response.body())

        request = HttpRequest.DELETE("/blogs/1")
        response= client.toBlocking().exchange(request)
        Assertions.assertEquals(HttpStatus.NO_CONTENT,response.status)
    }
}