package example.blog

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType

import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import org.junit.Test
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.function.Executable
import javax.inject.Inject


class BasicAuthTest {
    @Inject
    lateinit var embeddedServer: EmbeddedServer

    @Test
    fun varifyHttpBasicAuthWorks(){
        val client:RxHttpClient = embeddedServer.applicationContext.createBean(RxHttpClient::class.java, embeddedServer.url)

        //when accessing a secured URL without authenticating

        val e = Executable { client.toBlocking().exchange<Any,Any>(HttpRequest.GET<Any>("/").accept(MediaType.TEXT_PLAIN_TYPE)) }

        val thrown = Assertions.assertThrows(HttpClientResponseException::class.java,e)
        Assertions.assertEquals(thrown.status,HttpStatus.UNAUTHORIZED)

        val rsp = client.toBlocking().exchange(HttpRequest.GET<Any>("/")
                .accept(MediaType.TEXT_PLAIN_TYPE)
                .basicAuth("sherlock","password"),
                String::class.java)

        Assertions.assertEquals(rsp.status, HttpStatus.OK)
        Assertions.assertEquals(rsp.body.get(),"sherlock")
    }
}