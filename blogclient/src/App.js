import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import BlogDashboard from './Components/BlogDashboard';
import ErrorText from './Components/ErrorText';
import LoginForm from './Components/LoginForm';

class App extends Component {

  render(){   
    return (

     // <BlogDashboard/>
     // <LoginForm/>
    
      <Switch>
          <Route exact path='/login' component={LoginForm}  />
          <Route exact path='/blogs' component={BlogDashboard} />
          <Route component={ErrorText}/>
      </Switch>
    
     
    );
  }
}

export default App;
