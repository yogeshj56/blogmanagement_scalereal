import axios from 'axios';
import React, { Component } from 'react';
import { SERVER_URL } from "../config";
const { makeStyles, Container, TextField, Button } = require("@material-ui/core");

class AddBlog extends Component{
    constructor(props){
        super(props);
        this.state = {
            title:props.blog.id?props.blog.title:"",
            subtitle:props.blog.id?props.blog.subtitle:"",
            content:props.blog.id?props.blog.content:"",
            isSubmitted:false
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    
    
    handleClick = (e) => {
        e.preventDefault();
        var that = this;

        if(this.props.blog.id ===undefined)
        {
            axios({
                method: "post",
                url:`${SERVER_URL}/blogs`,
                data:{
                    title:this.state.title,
                    subtitle:this.state.subtitle,
                    content:this.state.content
                },
                config:{headers:{"Content-Type":"multipart/form-data"}},
                withCredentials:true,
                auth:{
                    username:"sherlock",
                    password:"password"
                }
            })
            .then(function(response){
                if(response){
                    //alert("in responce");
                    that.props.blogsFetch();
                    that.props.toggleForm();
                }
            })
            .catch(function(response){
                console.log(response);
            });  
            
        } else {
            axios({
                method: "put",
                url:`${SERVER_URL}/blogs/${this.props.blog.id}`,
                data:{
                    title:this.state.title,
                    subtitle:this.state.subtitle,
                    content:this.state.content
                },
                config:{headers:{"Content-Type":"multipart/form-data"}},
                withCredentials:true,
                auth:{
                    username:"sherlock",
                    password:"password"
                }
            })
            .then(function(response){
                if(response){
                    //alert("in responce");
                    that.props.blogsFetch();
                    that.props.toggleForm();
                }
            })
            .catch(function(response){
                console.log(response);
            });  
        }
    };

    handleInputChange = e =>{
        this.setState({
            //values:{...this.state.values,[e.target.name]:e.target.value}
            //value:e.target.value
            [e.target.name]:e.target.value
        });
    }

    useStyles = makeStyles((theme)=>({
        paper:{
            marginTop:theme.spacing(8),
            display:'flex',
            flexDirection:'column',
            alignItems:'center'
        },

        form:{
            width:'100%',
            marginTop:theme.spacing(1)
        },

        submit: {
            margin: theme.spacing(3, 2, 2)
        }

    }))
    
    render(){
        const classes = this.useStyles;

        return(
            <Container component="main" maxWidth="xs">
                <div className={classes.paper}>
                    <form
                        className={classes.form}
                        noValidate
                    >
                        <TextField 
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="title"
                            label="Title"
                            name="title"
                            autoComplete="title"
                            value={this.state.title}
                            onChange={this.handleInputChange}
                            autoFocus
                        />
                        <TextField 
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="subtitle"
                            label="Subtitle"
                            name="subtitle"
                            autoComplete="subtitle"
                            value={this.state.subtitle}
                            onChange={this.handleInputChange}
                        />
                        <TextField 
                            variant="outlined"
                            margin="normal"
                            multiline
                            rows={2}
                            fullWidth
                            label="Content"
                            id="content"
                            name="content"
                            value={this.state.content}
                            onChange={this.handleInputChange}
                        />

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            className={classes.submit}
                            onClick={this.handleClick}
                        >
                            Submit
                        </Button>

                    </form>
                </div>
            </Container>
        )
    }
}
export default AddBlog;