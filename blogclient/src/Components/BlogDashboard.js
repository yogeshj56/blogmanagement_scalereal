import { Checkbox, ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, FormControlLabel, IconButton, makeStyles, Typography } from "@material-ui/core";
import Container from '@material-ui/core/Container';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/DeleteForeverTwoTone';
import EditIcon from '@material-ui/icons/EditTwoTone';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import axios from "axios";
import React, { Component } from 'react';
import { SERVER_URL } from "../config";
import AddBlog from "./AddBlog";

class BlogDashboard extends Component {

    constructor(props){
        super(props);
        
        this.state = {
            blogs: [],
            unauthorised:true,
            modalVisible:false,
            showForm:false,
            selectedBlog:{}
        };

        this.handleCheck = this.handleCheck.bind(this);
        this.deleteBlock = this.deleteBlock.bind(this);
    }

    toggleAddBlockPop = (addEditStatus) =>{
        this.setState({
            showForm: !this.state.showForm,
            selectedBlog:addEditStatus===0 ? {} :this.state.selectedBlog
        });
    }

    componentDidMount(){
        this.fetchBlogs();
    }

    fetchBlogs = () => {
        let that = this;
        axios.get(`${SERVER_URL}/blogs`,{
            params:{},
            withCredentials:true,
            auth:{
                username:"sherlock",
                password:"password"
            },
        })
        .then((Response)=>{
            that.setState({
                blogs:Response.data,
                unauthorised:false,
            });
        })
        .catch((error) => {
            if(error.response && error.response.status === 401){
                that.setState({
                    unauthorised:true
                })
            }
        })
    }

    deleteBlock=()=>{
        let that = this;
        axios({
            method: "delete",
            url:`${SERVER_URL}/blogs/${this.state.selectedBlog.id}`,
            
            //config:{headers:{"Content-Type":"multipart/form-data"}},
            withCredentials:true,
            auth:{
                username:"sherlock",
                password:"password"
            }
        })
        .then(function(response){
            if(response){
                that.fetchBlogs();
            }
        })
        .catch(function(response){
            console.log(response);
        });  
    }

    useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
        },
        heading:{
            fontsize:theme.typography.pxToRem(15),
            fontWeight:theme.typography.fontWeightRegular,
        },
        addIcon:{
            '& > *':{
                margin: theme.spacing(5),
            }
        }
    }));

    handleCheck=(e,blog)=>{
        if(e.target.checked){
            this.setState({
                selectedBlog:blog
            })
        }
            
    }

render(){
    
    const {blogs} = this.state;
    const classes = this.useStyles;

    return( 
        <Container component="main" maxWidth='xs'>
        
        <div className={classes.addIcon}>

            <IconButton aria-label="New Blog">
                <AddIcon onClick={() => this.toggleAddBlockPop(0)}/>
             </IconButton>
            <IconButton aria-label="Edit Blog">
                <EditIcon onClick={() => this.toggleAddBlockPop(1)}/>
            </IconButton>
            <IconButton aria-label="Delete Blog">
            <DeleteIcon onClick={this.deleteBlock}/>
            </IconButton>
            
    
        </div>
        
        {this.state.showForm ?
                              <AddBlog 
                                    blogsFetch={this.fetchBlogs} 
                                    toggleForm={this.toggleAddBlockPop} 
                                    blog={this.state.selectedBlog}
                              /> 
                              :null }
        
        <div className={classes.root}>

            {blogs.length > 0 && 
                blogs.map((blog,index) =>(
                    <div>
                    <ExpansionPanel key={blog.id}>
                    <ExpansionPanelSummary key={blog.id_title}
                        expandIcon={<ExpandMoreIcon/>}
                        arial-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <FormControlLabel 
                            aria-label="Acknowledge"
                            onClick={(event) => event.stopPropagation()}
                            onFocus={(event)=> event.stopPropagation()}
                            control={<Checkbox onChange={(e)=>this.handleCheck(e,blog)} />}
                            label={blog.title}
                        />
                        
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails key={blog.id_content}>
                        <Typography color="textSecondary">
                            {blog.content}
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                    </div>
                )
                )
            } 
           
        </div>
        </Container>
    );
    }
}
export default BlogDashboard;