import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Alert from '@material-ui/lab/Alert';
import React from 'react';

export default function ErrorAlert(){
    
    const [open, setOpen] = React.useState(true);
    
    return(
     <Collapse in={open}>
     <Alert severity="error"
      action={
           <IconButton
               aria-label="close"
               color="inherit"
               size="small"
               onClick={()=>{
                   setOpen(false);
               }}
               >
            <CloseIcon fontSize="inherit"/>
            </IconButton>
       }
    >
    Unauthorize User Credentials
    </Alert>
    </Collapse>
    
    );
}