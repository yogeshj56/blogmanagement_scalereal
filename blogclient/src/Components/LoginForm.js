import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import axios from "axios";
import React, { Component } from 'react';
import { SERVER_URL } from "../config";
import ErrorAlert from "./AlertFile";
import BlogDashboard from './BlogDashboard';

class LoginForm extends Component {

  constructor(props){
    super(props);
    this.state = {
        username:"",
        password:"",
        isSubmitted: false,
        isError:false,
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.checkAuthentication = this.checkAuthentication.bind(this);
   // this.ErrorAlert = this.ErrorAlert.bind(this);
   //this.history = useHistory();
}

useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },

    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },

    form: {
        width: '100%',
        marginTop: theme.spacing(1)
    },

    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}));


checkAuthentication = (e) =>{
    e.preventDefault();

    let that = this;
    
    axios.get(`${SERVER_URL}/login`,{
            params:{},
            withCredentials:true,
            auth:{
                username: this.state.values.username,
                password: this.state.values.password
            }
        })
        .then((response) => {
            
            that.setState({
                isError:false,
                isSubmitted:true
            });        
        })
        .catch((error) => {
            //alert("second responce");
            if(error.response && error.response.status === 401){
                that.setState({
                    isError:true
                });                
            }
        });
} 

handleInputChange = e => {
    //e.preventDefault();
    this.setState({
        values: {...this.state.values,[e.target.name]: e.target.value}
    })
}

render(){   
    const classes = this.useStyles;
   
    return (
        
        <Container component="main" maxWidth="xs">

           {this.state.isError  &&  (  
          
             <ErrorAlert/>
             
            )}  
                                          
            <CssBaseline />

            {!this.state.isSubmitted ? (
                <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>

                <Typography component="h1" variant="h5">
                    Sign In
                </Typography>

                <form
                    className={classes.form}
                    onSubmit={this.checkAuthentication}
                    noValidate
                    //onFinish={this.checkAuthentication}
                >

                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="User Name"
                        name="username"
                        autoComplete="username"
                        onChange={this.handleInputChange}
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="password"
                        label="Password"
                        name="password"
                        type="password"
                        onChange={this.handleInputChange}
                        autoComplete="current-password"
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        onChange={this.handleInputChange}
                        label="Remember me"
                    />

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
                       </Button>

                    <Grid container>
                        <Grid item xs>
                            <Link href="#" variant="body2">
                                Forget password?
                                </Link>
                        </Grid>
                        <Grid item>
                            <Link href="#" variant="body2">
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        
            ):<BlogDashboard/>}
                
        </Container>
    );
    }
}
export default LoginForm;

